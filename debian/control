Source: python-trio-websocket
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Carsten Schoenert <c.schoenert@t-online.de>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-sphinxdoc <!nodoc>,
 python3-all,
Build-Depends-Indep:
 pybuild-plugin-pyproject,
 python-trio-doc <!nodoc>,
 python3-async-generator,
 python3-pytest <!nocheck>,
 python3-pytest-asyncio <!nocheck>,
 python3-pytest-trio <!nocheck>,
 python3-pytest-twisted <!nocheck>,
 python3-setuptools,
 python3-sphinx <!nodoc>,
 python3-sphinx-rtd-theme <!nodoc>,
 python3-sphinxcontrib.trio <!nodoc>,
 python3-trio,
 python3-trustme <!nocheck>,
 python3-wsproto <!nodoc>,
Rules-Requires-Root: no
Standards-Version: 4.7.1
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-trio-websocket
Vcs-Git: https://salsa.debian.org/python-team/packages/python-trio-websocket.git
Homepage: https://github.com/python-trio/trio-websocket

Package: python-trio-websocket-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Multi-Arch: foreign
Description: Server and client Python library of the WebSocket protocol (Documentation)
 This library implements both server and client aspects of the WebSocket
 protocol, striving for safety, correctness, and ergonomics. It is based on the
 wsproto project, which is a Sans-IO state machine that implements the majority
 of the WebSocket protocol, including framing, codecs, and events. This library
 handles I/O using the Trio framework. This library passes the Autobahn Test
 Suite.
 .
 This package installs the documentation for the library.

Package: python3-trio-websocket
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Description: Server and client Python library of the WebSocket protocol (Python3 version)
 This library implements both server and client aspects of the WebSocket
 protocol, striving for safety, correctness, and ergonomics. It is based on the
 wsproto project, which is a Sans-IO state machine that implements the majority
 of the WebSocket protocol, including framing, codecs, and events. This library
 handles I/O using the Trio framework. This library passes the Autobahn Test
 Suite.
 .
 This package contains the Python 3 version of the library.
